﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMenuStart : MonoBehaviour
{
    public GameObject panelRules;
    public GameObject panelControl;
    public string sceneNameToLoad;
//charge la scène de jeu
    public void StartGame()
    {
        SceneManager.LoadScene(sceneNameToLoad);
    }
//rend le panel des règles visible
    public void Rules()
    {
        panelRules.SetActive(true);
    }
//cache le panel de règles
    public void BackMenu()
    {
        panelRules.SetActive(false);
    }
//quitte l'application
    public void Quit()
    {
        Application.Quit();
    }

    public void PanelControlOn()
    {
        panelControl.SetActive(true);
    }

    public void PanelControlOff()
    {
        panelControl.SetActive(false);
    }
}
