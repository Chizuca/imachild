﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class AnimButtonStart : MonoBehaviour
{
    public Vector3 rotation1;
    public Vector3 rotation2;
    public Vector3 scale1;
    public Vector3 scale2;
    public float duration;

    private void Start()
    {
        Sequence();
    }

    void Sequence()
    {
        duration = Random.Range(1, 4);
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(gameObject.GetComponentInChildren<TextMeshProUGUI>().DOColor(Color.white, duration));
        mySequence.Append(transform.DORotate(rotation1, duration));
        mySequence.Append(transform.DORotate(rotation2, duration));
        mySequence.Append(transform.DOScale(scale1, duration));
        mySequence.Append(gameObject.GetComponentInChildren<TextMeshProUGUI>().DOColor(Color.red, duration));
        mySequence.Append(transform.DOScale(scale2, duration).OnComplete(()=> Sequence()));
    }
}