﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using DG.Tweening;
using UnityEngine;

public class BehaviourParents : MonoBehaviour
{
    private bool _detectObject;
    private bool _alreadyTouch = false;

    private GameObject _player;

    public GameObject gameObjectTimer;

    public GameObject point1;
    public GameObject point2;
    public GameObject point3;

    public GameObject porte;
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    public void Sequence()
    {
//cette séquence permet de faire avancer le parent, lui faire aller de droite a gauche puis de reculer       

            Sequence mySequence = DOTween.Sequence();
            mySequence.Append(transform.DOMove(point2.transform.position, 2));
            mySequence.Append(porte.transform.DORotate(new Vector3(-90, -115, 0), 2));
            mySequence.Append(transform.DOMove(point3.transform.position, 2));
            mySequence.Append(transform.DORotate(new Vector3(0, 50, 0), 2));
            mySequence.Append(transform.DORotate(new Vector3(0,-50,0),2));
            mySequence.Append(transform.DORotate(new Vector3(0,0,0),2));
            mySequence.AppendInterval(1);
            mySequence.Append(transform.DOMove(point2.transform.position, 2).OnComplete(()=>InitialyzeAlreadyTouch()));
            mySequence.Append(porte.transform.DORotate(new Vector3(-90, 0, 0), 2));
            mySequence.Append(transform.DOMove(point1.transform.position, 2).OnComplete(()=>InitialyzeAlreadyTouch()));
            mySequence.OnComplete(() => EndSequence(false));

    }
    
//si le parent reçoit l'information qu'il y a un objet qui ne devrait pas être là il va envoyer l'information au player qu'il perd une vie.
    public void EndSequence(bool value)
    {
        _detectObject = value;
        if (_detectObject)
        {
            if (_alreadyTouch == false)
            {    FindObjectOfType<AudioManager>().Play("Baffe");
                _player.GetComponent<health>().LifePlayer(1,false);
                _alreadyTouch=true;
            }
        }
    }
//réinitialise la variable qui autorise les parents a détecter de nouveau si un objet n'est pas caché et dit au timer de reprendre
    public void InitialyzeAlreadyTouch()
    {
        _alreadyTouch = false;
        gameObjectTimer.GetComponent<Timer>().allowTimer = true;
    }
    
}
