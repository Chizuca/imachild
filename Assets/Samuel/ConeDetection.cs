﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeDetection : MonoBehaviour
{
    public float maxDistance;

    public LayerMask layerCollision;

    private Vector3 _origin;
    
    // Update is called once per frame
    void Update()
    {
        //trace un raycast dans la direction du cube pour détecter si il y a un objet que le joueur devait cacher et envoie l'information au parent.
        _origin = transform.position;
        RaycastHit hit;
        if (Physics.Raycast(transform.position,transform.forward,out hit,maxDistance,layerCollision))
        {
            
            if (hit.transform.CompareTag("ShardPiece"))
            {
                GetComponentInParent<BehaviourParents>().EndSequence(true);
            }
        }
    }
    
    //permet de debug les traits dessiné pour s'assurer qu'il n'y ai pas d'erreur.
    private void OnDrawGizmosSelected()
    {
        Gizmos.color=Color.green;
        Gizmos.DrawRay(_origin,transform.forward*maxDistance);
    }
}
