﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GameOverJuice : MonoBehaviour
{
    public GameObject lastError;
    // Start is called before the first frame update
    void Start()
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(Camera.main.transform.DOMove(lastError.transform.position + new Vector3(0,5,0), 0.5f));
        mySequence.Append(Camera.main.transform.DORotate(new Vector3(90,0,0), 2));
        mySequence.Append(Camera.main.transform.DOMove(lastError.transform.position + new Vector3(0, 3, 0), 0.5f));     
        mySequence.Append(transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f),1));
        mySequence.Append(transform.DOScale(new Vector3(1, 1, 1), 1));
        
    }

}
