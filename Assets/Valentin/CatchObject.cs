﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchObject : MonoBehaviour
{
    public Transform rightArmTransform;
    public Transform leftArmTransform;

    public bool rightHandIsFree = true;
    public bool leftHandIsFree = true;
    //public bool isTakingObject = false;
    
    public GameObject grabedObjectRightArm;
    public GameObject grabedObjectLeftArm;
    
    //Variables de gestions du choix actif de la main et des membres (celle que le joueur va controller)
    public GameObject[] controllingParts;
    public GameObject controllingHand;
    public int indexPart = 0;
    public GameObject particles;
    public GameObject touchToGrab;
    
    
    // Start is called before the first frame update
    void Start()
    {
        //Vide certaines variables pour éviter les conflits

        grabedObjectRightArm = null;
        grabedObjectLeftArm = null;
    }

    // Update is called once per frame
    void Update()
    {
        //Prend par défaut la main gauche (index 0)
        controllingHand = controllingParts[indexPart];
            
        /*
        Vector3 temp = Input.mousePosition;
        temp.z = 10f;
        controllingHand.transform.position = Camera.main.ScreenToWorldPoint(temp);
        */
        
        //Changer de main
        if (Input.GetKeyDown(KeyCode.C))
        {
            switch (indexPart)
            {
                case 0:
                    indexPart = 1;
                    break;
                case 1:
                    indexPart = 0;
                    break;
                default:
                    break;
            }
        }

        //Si la main droite est active, relache l'objet tenu en main droite
        if (indexPart == 1)
        {
            if (Input.GetKeyDown(KeyCode.G))
            { 
                Debug.Log("Put object down from right hand"); 
                //Lance la fonction pour lacher l'objet de main droite
                if (grabedObjectRightArm)
                {
                    LetObjectRight();
                }
                
            }
        }
        //Si la main gauche est active, relache l'objet tenu en main gauche
        if (indexPart == 0)
        { 
            if (Input.GetKeyDown(KeyCode.G)) 
            { 
                Debug.Log("Put object down from left hand"); 
                //Lance la fonction pour lacher l'objet de main gauche
                if (grabedObjectLeftArm)
                {
                    LetObjectLeft();
                }
               
            }
        }

        //Si un objet est dans la main droite du joueur, ce dernier prend les coordonnées de la main droite
        if (grabedObjectRightArm)
        {
            grabedObjectRightArm.transform.position = rightArmTransform.transform.position;
        }

        //Si un objet est dans la main gauche du joueur, ce dernier prend les coordonnées de la main gauche
        if (grabedObjectLeftArm)
        {
            grabedObjectLeftArm.transform.position = leftArmTransform.transform.position;
        }

        
        
    }

    public void OnTriggerExit(Collider other)
    {
        if ((rightHandIsFree && other.gameObject.CompareTag("Catchable") ||
             rightHandIsFree && other.gameObject.CompareTag("ShardPiece")) && controllingHand.name == rightArmTransform.name)
        {
            if (particles)
            {
             //   if (other.gameObject.CompareTag("ShardPiece"))
               // {
                 

              //  }

                particles.SetActive(false);
                touchToGrab.SetActive(false);
            }
        }
        
        if ((rightHandIsFree && other.gameObject.CompareTag("Catchable") ||
             rightHandIsFree && other.gameObject.CompareTag("ShardPiece")) && controllingHand.name == leftArmTransform.name)
        {
            if (particles)
            {
             //   if (other.gameObject.CompareTag("ShardPiece"))
              //  {
                 

               // }
                particles.SetActive(false);
                touchToGrab.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Spike"))
        {
            FindObjectOfType<AudioManager>().Play("Bebe");
            GetComponent<health>().TakeDamage(0.5f);
        }
    }

    public void OnTriggerStay(Collider other)
    {
        //Si la main du joueur touche, le joueur peu attraper l'objet avec F, marche pour la main droite et gauche
        
        if ((rightHandIsFree && other.gameObject.CompareTag("Catchable") ||
            rightHandIsFree && other.gameObject.CompareTag("ShardPiece")) && controllingHand.name == rightArmTransform.name)
        {
            if (rightHandIsFree)
            {
              //  if (other.gameObject.CompareTag("ShardPiece"))
               // {
              
               // }

                particles.SetActive(true);
                touchToGrab.SetActive(true);
                particles.gameObject.transform.position = other.transform.position;
                touchToGrab.gameObject.transform.position = other.transform.position + new Vector3(0, 0.5f, 0);
                if (Input.GetKeyDown(KeyCode.F))
                {
                    if (other.gameObject.GetComponent<Rigidbody>())
                    {
                        grabedObjectRightArm = other.gameObject;
                        GrabObjectRight();
                        particles.SetActive(false);
                        touchToGrab.SetActive(false);
                    }
                }
            }
            else
            {
                Debug.Log("Your right hand is full");
            }
        }

        if ((leftHandIsFree && other.gameObject.CompareTag("Catchable") ||
            leftHandIsFree && other.gameObject.CompareTag("ShardPiece")) && controllingHand.name == leftArmTransform.name)
        {
            if (leftHandIsFree)
            {
                //if (other.gameObject.CompareTag("ShardPiece"))
               // {
              
               // }

                particles.SetActive(true);
                touchToGrab.SetActive(true);
                particles.gameObject.transform.position = other.transform.position;
                touchToGrab.gameObject.transform.position = other.transform.position + new Vector3(0, 0.5f, 0);
                if (Input.GetKeyDown(KeyCode.F))
                {
                    if (other.gameObject.GetComponent<Rigidbody>())
                    {
                        grabedObjectLeftArm = other.gameObject;
                        GrabObjectLeft();
                        particles.SetActive(false);
                        touchToGrab.SetActive(true);
                    }
                }
            }
            else
            {
                Debug.Log("Your left hand is full");
            }
        }
    }

    //Prend un object dans la main droite du joueur
    public void GrabObjectRight()
    {
        Debug.Log("Right");
        particles.SetActive(false);
        touchToGrab.SetActive(false);
        foreach (var colliders in grabedObjectRightArm.gameObject.GetComponents<SphereCollider>())
        {
            colliders.enabled = false;
        }
        foreach (var colliders in grabedObjectRightArm.gameObject.GetComponents<BoxCollider>())
        {
            colliders.enabled = false;
        }
        grabedObjectRightArm.gameObject.GetComponent<Rigidbody>().useGravity = false;
        grabedObjectRightArm.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        //isTakingObject = true;
        rightHandIsFree = false;
    }
    
    //Prend un object dans la main gauche du joueur
    public void GrabObjectLeft()
    {
        Debug.Log("Left");
        particles.SetActive(false);
        touchToGrab.SetActive(false);
        foreach (var colliders in grabedObjectLeftArm.gameObject.GetComponents<SphereCollider>())
        {
            colliders.enabled = false;
        }
        
        foreach (var colliders in grabedObjectLeftArm.gameObject.GetComponents<BoxCollider>())
        {
            colliders.enabled = false;
        }
        
        grabedObjectLeftArm.gameObject.GetComponent<Rigidbody>().useGravity = false;
        grabedObjectLeftArm.gameObject.GetComponent<Rigidbody>().isKinematic = true;

        //isTakingObject = true;
        leftHandIsFree = false;
    }

    //Laisse l'objet de la main droite par terre
    public void LetObjectRight()
    {
        foreach (var colliders in grabedObjectRightArm.gameObject.GetComponents<SphereCollider>())
        {
            colliders.enabled = true;
        }
        
        foreach (var colliders in grabedObjectRightArm.gameObject.GetComponents<BoxCollider>())
        {
            colliders.enabled = true;
        }
        grabedObjectRightArm.gameObject.GetComponent<Rigidbody>().useGravity = true;
        grabedObjectRightArm.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        particles.SetActive(false);
        touchToGrab.SetActive(false);
        grabedObjectRightArm = null;
        //isTakingObject = false;
        rightHandIsFree = true;
    }
    
    //Laisse l'objet de la main gauche par terre
    public void LetObjectLeft()
    {
        foreach (var colliders in grabedObjectLeftArm.gameObject.GetComponents<SphereCollider>())
        {
            colliders.enabled = true;
        }
        
        foreach (var colliders in grabedObjectLeftArm.gameObject.GetComponents<BoxCollider>())
        {
            colliders.enabled = true;
        }
        grabedObjectLeftArm.gameObject.GetComponent<Rigidbody>().useGravity = true;
        grabedObjectLeftArm.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        particles.SetActive(false);
        touchToGrab.SetActive(false);
        grabedObjectLeftArm = null;
        leftHandIsFree = true;
    }
}
