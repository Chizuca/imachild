﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject moveObject;
    public float yOffSet;
    private Camera camera;
    public CatchObject player;

    private void Start()
    {
        camera = Camera.main;
    }

    void Update()
    {
        moveObject = player.gameObject.GetComponent<CatchObject>().controllingHand;    
        MoveOnGroundLayer(moveObject);
    }

    public void MoveOnGroundLayer(GameObject obj)
    {
        RaycastHit hitSphere;
        
        RaycastHit hit;
        Ray ray = camera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
       
        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                moveObject.transform.position = hit.point + new Vector3(0, yOffSet, 0);
            }

            if (Physics.SphereCast(transform.position, 0.02f, Vector3.forward, out hitSphere))
            {
                if (hitSphere.collider.gameObject.tag == "Hand")
                {
                    Debug.Log("Touching");
                }
            }
        }
    }
}
