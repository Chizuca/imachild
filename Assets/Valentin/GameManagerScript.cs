﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
    public BrokenVaseVictory brokenVase;
    public GameObject victoryPanel;
    public TextMeshProUGUI remainingShard;

    // Start is called before the first frame update
    void Start()
    {
        victoryPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (brokenVase.nbRepairedShards >= 4)
        {
            VictoryPanel();
        }

        if (remainingShard)
        {
            remainingShard.text = brokenVase.nbRepairedShards + "/" + brokenVase.shardParts3D.Length;   
        }

    }
    
    public void VictoryPanel()
    {
        Time.timeScale = 0;
        victoryPanel.SetActive(true);
    }
}
