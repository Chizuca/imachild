﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenVaseVictory : MonoBehaviour
{
    public int nbRepairedShards;
    public GameObject[] shardParts3D;
    public GameObject particle;
  
    
    // Start is called before the first frame update
    void Start()
    {    
    
        //Désactive les parties du vase
        foreach (var shard in shardParts3D)
        {
            shard.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        particle = GameObject.FindWithTag("Particle");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (particle)
        {
            Debug.Log("Particle");
            particle.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("ShardPiece"))
        {
            if (shardParts3D[other.gameObject.GetComponent<BrokenVaseShard>().indexShard].activeInHierarchy == false)
            {
                shardParts3D[other.gameObject.GetComponent<BrokenVaseShard>().indexShard].gameObject.SetActive(true);
                nbRepairedShards += 1;
                Destroy(other.gameObject);
               
            }
        }
    }
}
