﻿using System;
using System.Collections;
using System.Diagnostics.Contracts;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class health : MonoBehaviour
{
    public float nbLifePt;
    
    public System.Collections.Generic.List<GameObject> coeur;
    public System.Collections.Generic.List<GameObject> halfHeart;
    public GameObject panelGameOver;
    public bool onDemiHeart;
    public bool dead=true;

    // Update is called once per frame
    void Update()
    {
        if (nbLifePt <= 0&&dead)
        {
            panelGameOver.SetActive(true);
            FindObjectOfType<AudioManager>().Stop();
            FindObjectOfType<AudioManager>().Play("GameOver");
            Time.timeScale = 0f;
            dead = false;
        }
    }
    public void LifePlayer(int value, bool punaise)
    {
        TakeDamage(value);
    }
//cette fonction va analyser combien de dégâts c'est pris le joueur et en fonction du nombre de vie qu'il avait, il va pouvoir analyser le nombre de sprite a l'écran pour en retirer le nombre voulu
    public void TakeDamage(float nbDamage)
    {
        Camera.main.transform.DOShakePosition(2, 0.5f);
        nbLifePt -= nbDamage;
        if (nbLifePt >= 0)
        {
            if (nbDamage == 0.5f&&!onDemiHeart)
            {
                Debug.Log("cas1");
                halfHeart[(int) Math.Ceiling(nbLifePt)-1].SetActive(true);
                coeur[(int) Math.Ceiling(nbLifePt) - 1].SetActive(false);
                onDemiHeart = !onDemiHeart;
            }
            else if (nbDamage == 0.5f && onDemiHeart)
            {
                Debug.Log("cas2");
                halfHeart[(int) Math.Ceiling(nbLifePt)].SetActive(false);
                onDemiHeart = !onDemiHeart;

            }
            else if (nbDamage == 1 && onDemiHeart)
            {
                Debug.Log("cas3");
                halfHeart[(int) Math.Ceiling(nbLifePt)].SetActive(false);
                halfHeart[(int) Math.Ceiling(nbLifePt)-1].SetActive(true);
                coeur[(int) Math.Ceiling(nbLifePt) - 1].SetActive(false);
            }
            else if (nbDamage == 1 && !onDemiHeart)
            {
                Debug.Log("cas4");
                coeur[(int) Math.Ceiling(nbLifePt) ].SetActive(false);
            }
        }

    }
}
//nota bene on aurait pu faire qu'une seule liste en sautant deux index lorsqu'il y avait 1 de dégâts et un index lorsqu'il y avait 0.5 Et en alternant dans la liste un sprite sur deux demi rempli et rempli.