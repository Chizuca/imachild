﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class smoothcaméra : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.125f;

    public Vector3 offset;

    void FixedUpdate()
    {
        // Set la caméra pour quelle sois plus smooth
        Vector3 desiredposition = target.position + offset;
        Vector3 smoothedPosition =
            Vector3.SmoothDamp(transform.position, desiredposition, ref desiredposition, smoothSpeed);
        transform.position = smoothedPosition;
        transform.LookAt(target);
    }

    public void Update()
    {
        RaycastHit hit;

        if (Physics.Linecast(transform.position, offset))
        {
            Debug.Log("hello");

        }
    }
}