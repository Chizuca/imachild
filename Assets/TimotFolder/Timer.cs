﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
   
    public float startingtime = 15f;
    [SerializeField] public TMP_Text timertext;
    public float currenttime ;
    public bool allowTimer;

    public GameObject _parent;
    // Start is called before the first frame update
  public  void Start()
    {
        currenttime = startingtime;
        _parent = GameObject.FindGameObjectWithTag("Parent");
        allowTimer = true;
    }

    // Update is called once per frame
    public void Update()
    {
        if (allowTimer)
        {
            // Gère le timer
            currenttime -= Time.deltaTime;
        }
        timertext.text = currenttime.ToString("0");
        
        //quand le timer est fini on lance la séquence
        if (currenttime <= 0 )
        {
            _parent.GetComponent<BehaviourParents>().Sequence();
            currenttime = startingtime;
            allowTimer = false;
        }
    }
}
