﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityStandardAssets.CrossPlatformInput;

public class Bouton : MonoBehaviour
{
    public GameObject panelPause;
    public string sceneNameToLoad;
    public static bool GameIspaused = false;
    public GameObject panelGameover;
    private void Start()
    {
        GameIspaused = false;
        panelGameover.SetActive(false);
        Time.timeScale = 1f;
    }

    public void PauseOn()
    {// Active le menu pause
        panelPause.SetActive(true);
        GameIspaused = true;
        Time.timeScale = 0;
    }

    public void PauseOff()
    {// Désactive le menu pause
        panelPause.SetActive(false);
        GameIspaused = false;
        Time.timeScale = 1;
    }

    public void Retry()
    {// Reload la scene
        Time.timeScale = 1;
        SceneManager.LoadScene(sceneNameToLoad);
    }

    public void Play()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(sceneNameToLoad);
    }

    public void Quit()
    {// Quitt l'appli
        Application.Quit();
    }

    private void Update()
    {// Lance la fonction PauseOn, et lance la fonction Pauseoff
        if (Input.GetButtonDown("Cancel")&&!GameIspaused)
        {
            PauseOn();
        }
        else if (Input.GetButtonDown("Cancel") && GameIspaused)
        {
            PauseOff();
        }
    }

    public void Menu()
    {// Load le menu
        SceneManager.LoadScene("MenuStart");
        Time.timeScale = 1f;
    }
}
